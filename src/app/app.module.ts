import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { MainHomePage } from '../pages/MainHome/mainhome';
import { CanteensMealsProvider } from '../providers/canteens-meals/canteens-meals';
import { CantCrastoPage } from '../pages/CantCrasto/cantcrasto';
import { CantSantiagoPage } from '../pages/CantSantiago/cantsantiago';
import { UnivRestPage } from '../pages/UnivRest/univrest';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    MyApp,
    MainHomePage,
    CantCrastoPage,
    CantSantiagoPage,
    UnivRestPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainHomePage,
    CantCrastoPage,
    CantSantiagoPage,
    UnivRestPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CanteensMealsProvider
  ]
})
export class AppModule {}
