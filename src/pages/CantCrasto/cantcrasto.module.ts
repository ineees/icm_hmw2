import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CantCrastoPage } from './cantcrasto';

@NgModule({
  declarations: [
    CantCrastoPage,
  ],
  imports: [
    IonicPageModule.forChild(CantCrastoPage),
  ],
})
export class CantCrastoPageModule {}
