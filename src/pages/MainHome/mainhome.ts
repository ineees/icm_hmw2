import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CantCrastoPage } from '../CantCrasto/cantcrasto';
import { CantSantiagoPage } from '../CantSantiago/cantsantiago';
import { UnivRestPage } from '../UnivRest/univrest';

@Component({
  selector: 'page-mainhome',
  templateUrl: 'mainhome.html'
})
export class MainHomePage {

  cantcrastoPage = CantCrastoPage;
  cantsantiagoPage = CantSantiagoPage;
  univrestPage = UnivRestPage;

  constructor(public navCtrl: NavController ) {
  }

}
