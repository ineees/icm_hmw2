import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CantSantiagoPage } from './cantsantiago';

@NgModule({
  declarations: [
    CantSantiagoPage,
  ],
  imports: [
    IonicPageModule.forChild(CantSantiagoPage),
  ],
})
export class CantSantiagoPageModule {}
