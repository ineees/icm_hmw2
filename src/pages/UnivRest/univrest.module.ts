import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnivRestPage } from './univrest';

@NgModule({
  declarations: [
    UnivRestPage,
  ],
  imports: [
    IonicPageModule.forChild(UnivRestPage),
  ],
})
export class UnivRestPageModule {}
